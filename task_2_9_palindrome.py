"""Task2
Створіть програму, яка перевіряє, чи є паліндромом введена фраза.
"""


def is_palindrome(string):
    """Checks that a phrase/word is a palindrome"""
    string = del_punctuations(string).lower()
    return string == string[::-1]


def del_punctuations(string):
    """Removes all non-letter characters from the string"""
    return "".join(list(filter(str.isalpha, string)))


# test
assert is_palindrome("Was it a car or a cat I saw?")
assert is_palindrome("Mr. Owl ate my metal worm")
assert is_palindrome('Level')
assert is_palindrome('radar')
assert not is_palindrome('Carry')
print("Everything works )")
