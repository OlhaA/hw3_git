"""Task3
Нехай на кожну сходинку можна стати з попередньої або переступивши через одну.
Визначте, скількома способами можна піднятися на задану сходинку.
"""


def count_ways(steps):
    """Count how many ways you can climb rungs."""
    if steps == 0:
        return 0

    # previous - the sum of variations of ways on the previous rung
    # next - the sum of variations of ways on the next rung
    previous, next = 0, 1
    for step in range(steps):
        previous, next = next, previous + next
    return next


# test
assert count_ways(7) == 21
assert count_ways(3) == 3
assert count_ways(0) == 0
assert count_ways(1) == 1
