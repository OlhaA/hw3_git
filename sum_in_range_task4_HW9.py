"""Task4
Напишіть рекурсивну функцію, яка обчислює суму натуральних чисел,
які входять до заданого проміжку.
"""


def get_sum_in_range(first, last):
    """Get the sum of numbers in the range from first to last"""
    if first == last:
        return last
    return first + get_sum_in_range(first + 1, last)


# test
assert get_sum_in_range(1, 5) == 15
assert get_sum_in_range(7, 10) == 34
